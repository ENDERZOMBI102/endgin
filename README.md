EndGin: ENDERZOMBI102's Plugin
-
A plugin for intellij-based IDEs that adds support for various projects types and languages

Features:
- Project Types:
  - LoaderComplex Addon
  - Amalgamation
  - Legacy Fabric
  - Minicraft Fabric
  - Brachyura
  - Architectury
  - Minecraft Cursed Legacy
  - EndLoader
  - EndC Project
  - Portal Manipulator Plugin
  - BEEmod 2 package
- Language/File Support:
  - EndC
  - E
  - Various BEEmod Formats:
    - info.txt
    - editoritems.txt
    - properties.txt
    - vbsp_config.cfg
    