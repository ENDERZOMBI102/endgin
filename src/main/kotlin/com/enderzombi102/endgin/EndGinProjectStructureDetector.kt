package com.enderzombi102.endgin

import com.intellij.ide.util.projectWizard.importSources.DetectedProjectRoot
import com.intellij.ide.util.projectWizard.importSources.ProjectStructureDetector
import java.io.File

class EndGinProjectStructureDetector : ProjectStructureDetector() {
	override fun detectRoots(
		dir: File,
		children: Array<out File>,
		base: File,
		result: MutableList<DetectedProjectRoot>
	): DirectoryProcessingResult {
		return DirectoryProcessingResult.SKIP_CHILDREN
	}
}