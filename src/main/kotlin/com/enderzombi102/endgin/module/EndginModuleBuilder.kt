package com.enderzombi102.endgin.module

import com.intellij.ide.util.projectWizard.*
import com.intellij.openapi.module.Module
import com.intellij.openapi.roots.ModifiableRootModel
import com.intellij.openapi.roots.ui.configuration.ModulesProvider
import java.util.*

class EndginModuleBuilder : JavaModuleBuilder(), ModuleBuilderListener {
	override fun getModuleType() = EndginModuleTypes.ENDGIN

	override fun setupRootModel(model: ModifiableRootModel ) {
		super.setupRootModel( model )
	}

	override fun createWizardSteps(
		ctx: WizardContext,
		provider: ModulesProvider
	): Array<ModuleWizardStep> {
		return arrayOf()
	}

	override fun moduleCreated( module: Module ) {
		TODO("Not yet implemented")
	}

	override fun modifySettingsStep( settingsStep: SettingsStep ): ModuleWizardStep? {
		return null
	}

	override fun getAdditionalFields(): MutableList< WizardInputField< * > > {
		return Collections.emptyList()
	}

}
