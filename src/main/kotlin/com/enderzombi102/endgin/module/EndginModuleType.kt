package com.enderzombi102.endgin.module

import com.enderzombi102.endgin.Icons
import com.intellij.openapi.module.ModuleType
import javax.swing.Icon

class EndginModuleType : ModuleType<EndginModuleBuilder>("ENDGIN_MODULE") {
	override fun createModuleBuilder(): EndginModuleBuilder {
		return EndginModuleBuilder()
	}

	override fun getName(): String {
		return "Endgin Module"
	}

	override fun getDescription(): String {
		return "Module for custom endgine templates"
	}

	override fun getNodeIcon(isOpened: Boolean): Icon {
		return Icons.PLUGIN_16_ICON
	}
}