package com.enderzombi102.endgin.facet

import com.intellij.facet.FacetType
import com.intellij.facet.Facet
import com.intellij.facet.FacetTypeId
import com.intellij.openapi.module.Module
import com.intellij.openapi.module.ModuleType

class EndginFacetType :
	FacetType<EndginFacet?, EndginFacetConfiguration>( ID, "com.enderzombi102.Facet", "FacetEndgin" ) {
	override fun createDefaultConfiguration(): EndginFacetConfiguration? {
		return null
	}

	override fun createFacet(
		module: Module,
		name: String,
		configuration: EndginFacetConfiguration,
		underlyingFacet: Facet<*>?
	): EndginFacet? {
		return null
	}

	override fun isSuitableModuleType(moduleType: ModuleType<*>?): Boolean {
		return false
	}

	companion object {
		val ID = FacetTypeId<EndginFacet?>("EndginD")
	}
}