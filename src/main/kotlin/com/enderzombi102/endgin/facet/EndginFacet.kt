package com.enderzombi102.endgin.facet

import com.intellij.facet.Facet
import com.intellij.openapi.module.Module

class EndginFacet(module: Module, name: String, configuration: EndginFacetConfiguration, underlyingFacet: Facet<*>?) :
	Facet<EndginFacetConfiguration?>(
		TYPE, module, name, configuration, underlyingFacet
	) {
	companion object {
		val TYPE = EndginFacetType()
	}
}