package com.enderzombi102.endgin

import com.intellij.ui.IconManager
import javax.swing.Icon

object Icons {
	val PLUGIN_16_ICON = load("Endgin16.png")
	val PLUGIN_32_ICON = load("Endgin32.png")

	// Fabric
	val ARCHITECTURY_ICON = load("Achitectury.png")
	val ARCHITECTURY_16_ICON = load("Achitectury16.png")
	val ARCHITECTURY_32_ICON = load("Achitectury32.png")

	val CURSED_LEGACY_ICON = load("CursedLegacy.png")
	val CURSED_LEGACY_16_ICON = load("CursedLegacy16.png")
	val CURSED_LEGACY_32_ICON = load("CursedLegacy32.png")

	val LEGACY_FABRIC_ICON = load("LegacyFabric.png")
	val LEGACY_FABRIC_16_ICON = load("LegacyFabric16.png")
	val LEGACY_FABRIC_32_ICON = load("LegacyFabric32.png")

	val MINI_FABRIC_ICON = load("MiniFabric.png")
	val MINI_FABRIC_16_ICON = load("MiniFabric16.png")
	val MINI_FABRIC_32_ICON = load("MiniFabric32.png")

	// Languages
	val SQUIRREL_ICON = load("Squirrel.png")
	val SQUIRREL_16_ICON = load("Squirrel16.png")
	val SQUIRREL_32_ICON = load("Squirrel32.png")

	val ANGELSCRIPT_ICON = load("AngelScript.png")

	// Source
	val BEEmod_16_ICON = load("BEEmod16.png")
	val BEEmod_32_ICON = load("BEEmod32.png")

	private fun load(path: String): Icon {
		return IconManager.getInstance().getIcon("assets/endgin/icons/$path", Icons::class.java)
	}
}